#include <iostream>

#include "ctm/base.hpp"
#include "ctm/arithmetic.hpp"
#include "ctm/detail/lshift.hpp"
#include "ctm/detail/bitwise.hpp"

#include <typeinfo>

#include <cxxabi.h>

template <typename T>
const char *n(T)
{
    return abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, nullptr);
}

int main()
{
    using namespace ctm::literals;

    using a = ctm::integer<false, 0x01>;
    using b = ctm::integer<false>;
    using c = ctm::integer<false, 0xff>;
    using d = ctm::integer<false, 0xff, 0xff>;
    using e = ctm::integer<true, 0x01>;
    using f = ctm::integer<true, 0xff, 0xff>;
    using g = ctm::integer<true, 0x00, 0xff>;

    std::cout << "add" << std::endl;

    std::cout << n(ctm::detail::add<a, b>::type{}) << std::endl;
    std::cout << n(ctm::detail::add<a, c>::type{}) << std::endl;
    std::cout << n(ctm::detail::add<c, b>::type{}) << std::endl;
    std::cout << n(ctm::detail::add<a, d>::type{}) << std::endl;
    std::cout << n(ctm::detail::add<c, d>::type{}) << std::endl;
    std::cout << n(a{} + d{}) << std::endl;
    std::cout << n(ctm::detail::add<e, f>::type{}) << std::endl;
    std::cout << n(a{} + d{}) << std::endl;
    std::cout << n(f{} + c{}) << std::endl;
    std::cout << n(-f{} + -c{}) << std::endl;
    std::cout << n(-f{} - c{}) << std::endl;
    std::cout << n(d{} + g{}) << std::endl;
    std::cout << n(0x10_ctm_i + 0x0f_ctm_i) << std::endl;

    std::cout << "lshift" << std::endl;

    std::cout << n(ctm::detail::lshift<a, 2>::type{}) << std::endl;
    std::cout << n(ctm::detail::lshift<d, 3>::type{}) << std::endl;
    std::cout << n(ctm::detail::lshift<d, 8>::type{}) << std::endl;

    std::cout << n(0xfe_ctm_i) << std::endl;
    std::cout << n(0x1fe_ctm_i) << std::endl;
    std::cout << ctm::detail::ffs<g>::value << std::endl;

    std::cout << n(0xfe_ctm_i * 0x2_ctm_i) << std::endl;
}
