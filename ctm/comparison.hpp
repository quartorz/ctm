#pragma once

#include "detail/comparison.hpp"

namespace ctm{
    using detail::compare;
}
