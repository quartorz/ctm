#pragma once

#include "base.hpp"

namespace ctm{
    namespace detail{
        // find first set bit
        template <typename>
        class ffs;

        template <bool Neg, uint_t... N>
        class ffs<integer<Neg, N...>>{
            template <typename, unsigned>
            struct ffs_i;

            template <unsigned Y>
            struct ffs_i<integer<Neg>, Y>{
                static constexpr unsigned value = Y;
            };

            template <uint_t X, uint_t... Xs, unsigned Y>
            struct ffs_i<integer<Neg, X, Xs...>, Y>{
                static constexpr unsigned value =
                    X != 0
                        ? __builtin_ffs(X) + Y
                        : ffs_i<integer<Neg, Xs...>, Y + uint_t_digits>::value;
            };

        public:
            static constexpr unsigned value = ffs_i<integer<Neg, N...>, 0>::value;
        };
    }
}
