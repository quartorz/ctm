#pragma once

#include "base.hpp"

#include <type_traits>

#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable : 4309)
#else
#pragma GCC diagnostic ignored "-Woverflow"
#if defined(__clang__)
#pragma GCC diagnostic ignored "-Wc++11-narrowing"
#endif
#endif

namespace ctm{
    namespace detail{
        template <typename, unsigned>
        class lshift;

        template <bool Neg, uint_t... N, unsigned M>
        class lshift<integer<Neg, N...>, M>{
            template <typename, unsigned, uint_t, typename = void>
            struct lshift_i;

            template <unsigned Y, uint_t Carry>
            struct lshift_i<integer<Neg>, Y, Carry, void>{
                using type = typename ::std::conditional<
                    Carry != 0,
                    integer<Neg, Carry>,
                    integer<Neg>
                >::type;
            };

            template <uint_t X, uint_t... Xs, unsigned Y, uint_t Carry>
            struct lshift_i<
                integer<Neg, X, Xs...>,
                Y,
                Carry,
                typename ::std::enable_if<Y == 0>::type
            >
            {
                using type = integer<Neg, X, Xs...>;
            };

            template <uint_t X, uint_t... Xs, unsigned Y, uint_t Carry>
            struct lshift_i<
                integer<Neg, X, Xs...>,
                Y,
                Carry,
                typename ::std::enable_if<Y != 0 && Y / uint_t_digits == 0>::type
            >
            {
                using type = typename cons<
                    (X << Y) | Carry,
                    typename lshift_i<integer<Neg, Xs...>, Y, (X >> (uint_t_digits - Y))>::type
                >::type;
            };

            template <uint_t X, uint_t... Xs, unsigned Y, uint_t Carry>
            struct lshift_i<
                integer<Neg, X, Xs...>,
                Y,
                Carry,
                typename ::std::enable_if<Y / uint_t_digits != 0>::type
            >
            {
                using type = typename cons<
                    0 | Carry,
                    typename lshift_i<integer<Neg, X, Xs...>, Y - uint_t_digits, 0>::type
                >::type;
            };

        public:
            using type = typename lshift_i<integer<Neg, N...>, M, 0>::type;
        };

        /*template <bool Neg, uint_t... N>
        constexpr auto operator<<(integer<Neg, N...>, unsigned x)
            -> typename lshift<integer<Neg, N...>, x>::type
        {
            return {};
        }*/
    }
}

#if defined(_MSC_VER)
#pragma warning(pop)
#else
#pragma GCC diagnostic warning "-Woverflow"
#if defined(__clang__)
#pragma GCC diagnostic warning "-Wc++11-narrowing"
#endif
#endif
