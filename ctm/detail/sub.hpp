#pragma once

#include "base.hpp"
#include "add.hpp"

namespace ctm{
    namespace detail{
        template <typename Left, typename Right>
        using sub = typename add<Left, typename negate<Right>::type>::type;

        template <bool S1, uint_t... N, bool S2, uint_t... M>
        constexpr typename add<integer<S1, N...>, integer<!S2, M...>>::type
        operator-(integer<S1, N...>, integer<S2, M...>)
        {
            return {};
        }
    }
}
