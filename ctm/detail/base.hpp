#pragma once

#include <cstdint>
#include <limits>
#include <type_traits>

namespace ctm{
    namespace detail{
        using uint_t = ::std::uint8_t;

        constexpr auto uint_t_digits = ::std::numeric_limits<uint_t>::digits;
        constexpr auto uint_t_max = ::std::numeric_limits<uint_t>::max();

        constexpr bool overflow(uint_t x, uint_t y)
        {
            return x + y < x;
        }

        constexpr bool overflow(uint_t x, uint_t y, bool carry)
        {
            return carry ? overflow(x, y) || x + y + 1 == 0 : overflow(x, y);
        }

        template <bool Neg, uint_t N>
        struct singleint{
            constexpr bool negative() const
            {
                return Neg;
            }
        };

        template <bool Neg, uint_t... N>
        struct integer{
            constexpr bool negative() const
            {
                return Neg;
            }
        };

        template <typename>
        struct negate;
        template <bool Neg, uint_t N>
        struct negate<singleint<Neg, N>>{
            using type = singleint<!Neg, N>;
        };
        template <bool Neg, uint_t... N>
        struct negate<integer<Neg, N...>>{
            using type = integer<!Neg, N...>;
        };

        template <typename>
        struct digits{
        };
        template <bool Neg, uint_t N>
        struct digits<singleint<Neg, N>>{
            static constexpr unsigned value = uint_t_digits;
        };
        template <bool Neg>
        struct digits<integer<Neg>>{
            static constexpr unsigned value = 0;
        };
        template <bool Neg, uint_t N, uint_t... Ns>
        struct digits<integer<Neg, N, Ns...>>{
            static constexpr unsigned value = uint_t_digits + digits<integer<Neg, Ns...>>::value;
        };

        template <typename, typename>
        struct append;
        template <bool Neg, uint_t... N, uint_t... M>
        struct append<integer<Neg, N...>, integer<Neg, M...>>{
            using type = integer<Neg, N..., M...>;
        };

        template <uint_t, typename>
        struct cons;
        template <bool Neg>
        struct cons<0, integer<Neg>>{
            using type = integer<Neg>;
        };
        template <uint_t N, bool Neg, uint_t... M>
        struct cons<N, integer<Neg, M...>>{
            using type = integer<Neg, N, M...>;
        };

        template <typename, typename = void>
        struct strip;
        template <bool Neg>
        struct strip<integer<Neg>, void>{
            using type = integer<Neg>;
        };
        template <bool Neg, uint_t N, uint_t... Ns>
        struct strip<
            integer<Neg, N, Ns...>,
            typename ::std::enable_if<
                ::std::is_same<integer<Neg>, typename strip<integer<Neg, Ns...>>::type>::value
            >::type
        >{
            using type = typename ::std::conditional<
                N == 0,
                integer<Neg>,
                integer<Neg, N>
            >::type;
        };
        template <bool Neg, uint_t N, uint_t... Ns>
        struct strip<
            integer<Neg, N, Ns...>,
            typename ::std::enable_if<
                !::std::is_same<integer<Neg>, typename strip<integer<Neg, Ns...>>::type>::value
            >::type
        >{
            using type = typename cons<N, typename strip<integer<Neg, Ns...>>::type>::type;
        };

        template <bool Neg, uint_t N>
        constexpr singleint<!Neg, N> operator-(singleint<Neg, N>)
        {
            return {};
        }

        template <bool Neg, uint_t... N>
        constexpr integer<!Neg, N...> operator-(integer<Neg, N...>)
        {
            return {};
        }
    }

    namespace literals{
        namespace detail{
            using uint_t = ::ctm::detail::uint_t;

            constexpr auto uint_t_digits = ::ctm::detail::uint_t_digits;

            constexpr uint_t to_int(uint_t n)
            {
                return ('0' <= n && n <= '9')
                    ? n - '0'
                    : (n & 0xf) + 9;
            }

            constexpr uint_t pow(uint_t x, uint_t n)
            {
                return n == 0 ? 1 : x * pow(x, n - 1);
            }

            template <uint_t Holder, uint_t Count, typename, uint_t...>
            struct fromhex;

            template <uint_t Holder, uint_t Count>
            struct fromhex<Holder, Count,  void>{
                using type = ::ctm::detail::integer<false, Holder>;
            };

            template <uint_t Holder, uint_t Count, uint_t N, uint_t... Ns>
            struct fromhex<
                Holder,
                Count,
                typename ::std::enable_if<Count == uint_t_digits / 4>::type,
                N,
                Ns...
            >{
                using type = typename ::ctm::detail::append<typename fromhex<0, 0, void, N, Ns...>::type, ::ctm::detail::integer<false, Holder>>::type;
            };

            template <uint_t Holder, uint_t Count, uint_t N, uint_t... Ns>
            struct fromhex<
                Holder,
                Count,
                typename ::std::enable_if<Count != uint_t_digits / 4>::type,
                N,
                Ns...
            >{
                using type = typename fromhex<(Holder << 4) | N, Count + 1, void, Ns...>::type;
            };

            // hexadecimal
            template <char A, char B, char... Rest>
            constexpr typename ::ctm::detail::strip<typename ::std::enable_if<
                A == '0' && (B == 'x' || B == 'X'),
                fromhex<
                    0,
                    (uint_t_digits / 4) - sizeof...(Rest) % (uint_t_digits / 4),
                    void,
                    to_int(Rest)...
                >
            >::type::type>::type parse()
            {
                return {};
            }

            // octal
            template <char A, char B, char... Rest>
            constexpr typename ::std::enable_if<
                A == '0' && !(B == 'x' || B == 'X'),
                void
            >::type parse()
            {
                return {};
            }

            // binary
            template <char A, char B, char... Rest>
            constexpr typename ::std::enable_if<
                A == '0' && (B == 'b' || B == 'B'),
                void
            >::type parse()
            {
                return {};
            }

            // zero
            template <char A>
            constexpr typename ::std::enable_if<
                A == '0',
                ::ctm::detail::integer<false>
            >::type parse()
            {
                return {};
            }

            // decimal
            template <char A, char... Rest>
            constexpr typename ::std::enable_if<
                A != '0',
                void
            >::type parse()
            {
                return {};
            }
        }

        /* template <char... Chars>
        constexpr decltype(detail::parse<Chars...>()) operator"" _ctm_si()
        {
            return {};
        }*/

        template <char... Chars>
        constexpr decltype(detail::parse<Chars...>()) operator"" _ctm_i()
        {
            return {};
        }
    }
}
