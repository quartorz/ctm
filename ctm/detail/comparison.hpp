#pragma once

#include "base.hpp"

#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable : 4309)
#else
#pragma GCC diagnostic ignored "-Woverflow"
#if defined(__clang__)
#pragma GCC diagnostic ignored "-Wc++11-narrowing"
#endif
#endif

namespace ctm{
    namespace detail{
        template <typename, typename>
        struct compare;

        template <uint_t... N, uint_t... M>
        struct compare<integer<false, N...>, integer<true, M...>>{
            static constexpr int value = (sizeof...(N) == 0 && sizeof...(M) == 0) ? 0 : 1;
        };
        template <uint_t... N, uint_t... M>
        struct compare<integer<true, N...>, integer<false, M...>>{
            static constexpr int value = (sizeof...(N) == 0 && sizeof...(M) == 0) ? 0 : -1;
        };

        template <>
        struct compare<integer<false>, integer<false>>{
            static constexpr int value = 0;
        };
        template <uint_t N, uint_t... Ns>
        struct compare<integer<false, N, Ns...>, integer<false>>{
            static constexpr int value = 1;
        };
        template <uint_t M, uint_t... Ms>
        struct compare<integer<false>, integer<false, M, Ms...>>{
            static constexpr int value = -1;
        };
        template <uint_t N, uint_t... Ns, uint_t M, uint_t... Ms>
        struct compare<integer<false, N, Ns...>, integer<false, M, Ms...>>{
            static constexpr int value =
                compare<integer<false, Ns...>, integer<false, Ms...>>::value == 0
                    ? (N < M ? -1 : !!(N - M))
                    : compare<integer<false, Ns...>, integer<false, Ms...>>::value;
        };
        template <uint_t... N, uint_t... M>
        struct compare<integer<true, N...>, integer<true, M...>>{
            static constexpr int value = - compare<integer<false, N...>, integer<false, M...>>::value;
        };

        template <bool S1, uint_t... N, bool S2, uint_t... M>
        constexpr bool operator==(integer<S1, N...>, integer<S2, M...>)
        {
            return compare<integer<S1, N...>, integer<S2, M...>>::value == 0;
        }

        template <bool S1, uint_t... N, bool S2, uint_t... M>
        constexpr bool operator>(integer<S1, N...>, integer<S2, M...>)
        {
            return compare<integer<S1, N...>, integer<S2, M...>>::value == 1;
        }

        template <bool S1, uint_t... N, bool S2, uint_t... M>
        constexpr bool operator<(integer<S1, N...>, integer<S2, M...>)
        {
            return compare<integer<S1, N...>, integer<S2, M...>>::value == -1;
        }

        template <bool S1, uint_t... N, bool S2, uint_t... M>
        constexpr bool operator>=(integer<S1, N...>, integer<S2, M...>)
        {
            return compare<integer<S1, N...>, integer<S2, M...>>::value >= 1;
        }

        template <bool S1, uint_t... N, bool S2, uint_t... M>
        constexpr bool operator<=(integer<S1, N...>, integer<S2, M...>)
        {
            return compare<integer<S1, N...>, integer<S2, M...>>::value <= -1;
        }
    }
}

#if defined(_MSC_VER)
#pragma warning(pop)
#else
#pragma GCC diagnostic warning "-Woverflow"
#if defined(__clang__)
#pragma GCC diagnostic warning "-Wc++11-narrowing"
#endif
#endif
