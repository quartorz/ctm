#pragma once

#include "base.hpp"
#include "comparison.hpp"

#include <type_traits>

#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable : 4309)
#else
#pragma GCC diagnostic ignored "-Woverflow"
#if defined(__clang__)
#pragma GCC diagnostic ignored "-Wc++11-narrowing"
#endif
#endif

namespace ctm{
    namespace detail{
        template <typename, typename>
        class add;

        template <uint_t... N, uint_t... M>
        class add<integer<false, N...>, integer<false, M...>>{
            template <typename, typename, bool Carry>
            struct add_i{
            };

            template <bool Carry>
            struct add_i<integer<false>, integer<false>, Carry>{
                using type = typename ::std::conditional<
                    Carry,
                    integer<false, 1>,
                    integer<false>
                >::type;
            };

            template <uint_t X, uint_t... Xs, bool Carry>
            struct add_i<integer<false, X, Xs...>, integer<false>, Carry>{
                using type = typename cons<
                    X + (Carry ? 1 : 0),
                    typename add_i<
                        integer<false, Xs...>,
                        integer<false>,
                        overflow(X, (Carry ? 1 : 0))
                    >::type
                >::type;
            };

            template <uint_t Y, uint_t... Ys, bool Carry>
            struct add_i<integer<false>, integer<false, Y, Ys...>, Carry>
                : add_i<integer<false, Y, Ys...>, integer<false>, Carry>{
            };

            template <uint_t X, uint_t... Xs, uint_t Y, uint_t... Ys, bool Carry>
            struct add_i<integer<false, X, Xs...>, integer<false, Y, Ys...>, Carry>{
                using type = typename cons<
                    X + Y + (Carry ? 1 : 0),
                    typename add_i<
                        integer<false, Xs...>,
                        integer<false, Ys...>,
                        overflow(X, Y, Carry)
                    >::type
                >::type;
            };

        public:
            using type = typename add_i<integer<false, N...>, integer<false, M...>, false>::type;
        };
        template <uint_t... N, uint_t... M>
        class add<integer<true, N...>, integer<true, M...>>{
        public:
            using type = typename negate<typename add<integer<false, N...>, integer<false, M...>>::type>::type;
        };

        template <uint_t... N, uint_t... M>
        class add<integer<false, N...>, integer<true, M...>>{
            template <typename, typename, bool Borrow>
            struct add_ii{
            };
            template <bool Borrow>
            struct add_ii<integer<false>, integer<true>, Borrow>{
                using type = integer<false>;
            };
            template <uint_t X, uint_t... Xs, bool Borrow>
            struct add_ii<integer<false, X, Xs...>, integer<true>, Borrow>{
                using type = integer<false, X - (Borrow ? 1 : 0), Xs...>;
            };
            template <uint_t X, uint_t... Xs, uint_t Y, uint_t... Ys, bool Borrow>
            struct add_ii<integer<false, X, Xs...>, integer<true, Y, Ys...>, Borrow>{
                using type =
                    typename cons<
                        (X >= (Y + (Borrow ? 1 : 0))
                            ? X - Y - (Borrow ? 1 : 0)
                            : X - Y - (Borrow ? 1 : 0) + 1 + (uint_t)-1),
                        typename add_ii<integer<false, Xs...>, integer<true, Ys...>, (X < Y + (Borrow ? 1 : 0))>::type
                    >::type;
            };

            template <typename, typename, typename = void>
            struct add_i{
            };
            template <uint_t... X, uint_t... Y>
            struct add_i<
                integer<false, X...>,
                integer<true, Y...>,
                typename ::std::enable_if<
                    compare<integer<false, X...>, integer<false, Y...>>::value == 1>::type
            >
            {
                using type = typename add_ii<integer<false, X...>, integer<true, Y...>, false>::type;
            };
            template <uint_t... X, uint_t... Y>
            struct add_i<
                integer<false, X...>,
                integer<true, Y...>,
                typename ::std::enable_if<
                    compare<integer<false, X...>, integer<false, Y...>>::value == -1>::type
            >
            {
                using type = typename negate<typename add_ii<integer<false, Y...>, integer<true, X...>, true>::type>::type;
            };
            template <uint_t... X, uint_t... Y>
            struct add_i<
                integer<false, X...>,
                integer<true, Y...>,
                typename ::std::enable_if<
                    compare<integer<false, X...>, integer<false, Y...>>::value == 0>::type
            >
            {
                using type = integer<false>;
            };

        public:
            using type = typename add_i<integer<false, N...>, integer<true, M...>>::type;
        };
        template <uint_t... N, uint_t... M>
        class add<integer<true, N...>, integer<false, M...>>{
        public:
            using type = typename negate<typename add<integer<false, N...>, integer<true, M...>>::type>::type;
        };

        template <bool S1, uint_t... N, bool S2, uint_t... M>
        constexpr typename add<integer<S1, N...>, integer<S2, M...>>::type
        operator+(integer<S1, N...>, integer<S2, M...>)
        {
            return {};
        }
    }
}

#if defined(_MSC_VER)
#pragma warning(pop)
#else
#pragma GCC diagnostic warning "-Woverflow"
#if defined(__clang__)
#pragma GCC diagnostic warning "-Wc++11-narrowing"
#endif
#endif
