#pragma once

#include "base.hpp"
#include "add.hpp"
#include "sub.hpp"
#include "lshift.hpp"
#include "bitwise.hpp"

namespace ctm{
    namespace detail{
        template <typename, typename>
        class mul;

        template <bool Neg, uint_t... N, uint_t... M>
        class mul<integer<Neg, N...>, integer<Neg, M...>>{
            template <typename, typename>
            struct mul_i;

            template <uint_t... X>
            struct mul_i<integer<Neg, X...>, integer<Neg>>{
                using type = integer<false>;
            };

            template <uint_t Y, uint_t... Ys>
            struct mul_i<integer<Neg>, integer<Neg, Y, Ys...>>{
                using type = integer<false>;
            };

            template <uint_t X, uint_t... Xs, uint_t Y, uint_t... Ys>
            struct mul_i<integer<Neg, X, Xs...>, integer<Neg, Y, Ys...>>{
                using type = typename add<
                    typename lshift<integer<Neg, X, Xs...>, ffs<integer<Neg, Y, Ys...>>::value - 1>::type,
                    typename mul_i<
                        integer<Neg, X, Xs...>,
                        typename add<
                            integer<Neg, Y, Ys...>,
                            typename lshift<
                                integer<!Neg, 1>,
                                ffs<integer<Neg, Y, Ys...>>::value - 1
                            >::type
                        >::type
                    >::type
                >::type;
            };

        public:
            using type = typename mul_i<integer<Neg, N...>, integer<Neg, M...>>::type;
        };

        template <uint_t... N, uint_t... M>
        class mul<integer<false, N...>, integer<true, M...>>{
            using type = typename negate<typename mul<integer<false, N...>, integer<false, M...>>::type>::type;
        };

        template <uint_t... N, uint_t... M>
        class mul<integer<true, N...>, integer<false, M...>>{
            using type = typename negate<typename mul<integer<false, N...>, integer<false, M...>>::type>::type;
        };

        template <bool S1, uint_t... N, bool S2, uint_t... M>
        constexpr typename mul<integer<S1, N...>, integer<S2, M...>>::type operator*(integer<S1, N...>, integer<S2, M...>)
        {
            return {};
        }
    }
}
