#pragma once

#include "detail/add.hpp"
#include "detail/sub.hpp"
#include "detail/mul.hpp"

namespace ctm{
    using detail::add;
    using detail::sub;
    using detail::mul;
}
